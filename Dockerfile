FROM alpine:3.18

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ENV NGINX_VERSION 1.24.0

USER 0

RUN adduser -D -S -h /home/nginx -s /sbin/nologin -G root --uid 1001 nginx && \
    apk update && \
    apk upgrade && \
    apk add \
        bash \
        file \
        grep \
        tree \
        make \
        git \
        gcc \
        pcre \
        pcre-dev \
        libc-dev \
        zlib-dev \
        libxml2 \
        libxml2-dev \
        libxslt \
        libxslt-dev && \
    mkdir /home/nginx/sbin && \
    mkdir /home/nginx/pid && \
    mkdir /home/nginx/logs && \
    cd /tmp && \
    git clone https://github.com/arut/nginx-dav-ext-module.git && \
    cd nginx-dav-ext-module && \
    git checkout v3.0.0 && \
    cd /tmp && \
    wget http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz && \
    tar xfz nginx-$NGINX_VERSION.tar.gz && \
    cd nginx-$NGINX_VERSION && \
    ./configure \
        --prefix=/home/nginx \
        --sbin-path=sbin \
        --conf-path=conf/server.conf \
        --pid-path=pid/nginx.pid \
        --with-http_gzip_static_module \
        --with-http_gunzip_module \
        --with-http_auth_request_module \
        --with-http_xslt_module \
        --with-http_dav_module \
        --add-module=/tmp/nginx-dav-ext-module && \
    make && \
    make install && \
    cd /tmp && \
    rm -rf ./* && \
    apk del \
        make \
        git \
        gcc \
        pcre-dev \
        libc-dev \
        zlib-dev \
        libxml2-dev \
        libxslt-dev && \
    ln -sf /dev/stdout /home/nginx/logs/access.log && \
	ln -sf /dev/stderr /home/nginx/logs/error.log && \
    chown -R 1001:0 /home/nginx && \
    chmod -R g=u /home/nginx && \
    mkdir /data && \
    chown -R 1001:0 /data && \
    chmod -R g=u /data && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd
    
COPY --chown=1001:0 server.conf /home/nginx/conf/server.conf
COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

USER 1001

WORKDIR /data

RUN tree /home/nginx

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

STOPSIGNAL SIGTERM

CMD ["-g", "daemon off;"]
